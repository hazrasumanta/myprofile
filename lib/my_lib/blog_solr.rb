module BlogSolr
	def search_solr(search_key)
		@search = BlogPost.solr_search do |s|
			s.fulltext search_key
			s.keywords search_key
			s.order_by :created_at, :desc
			s.with(:status, true)
			s.paginate :page => params[:page], :per_page => 5
		end
		return @search
	end
end