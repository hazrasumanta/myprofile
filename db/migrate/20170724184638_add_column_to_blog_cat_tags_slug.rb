class AddColumnToBlogCatTagsSlug < ActiveRecord::Migration[5.1]
  def change
    add_column :blog_cat_tags, :slug, :string
  end
end
