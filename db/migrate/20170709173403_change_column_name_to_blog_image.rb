class ChangeColumnNameToBlogImage < ActiveRecord::Migration[5.1]
  def change
  	rename_column :blog_images, :b_id, :blog_post_id
  end
end
