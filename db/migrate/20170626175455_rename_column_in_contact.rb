class RenameColumnInContact < ActiveRecord::Migration[5.1]
  def change
  	rename_column :contacts, :c_date, :c_data
  end
end
