class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.attachment :pic
      t.string :duration
      t.text :desc
      t.string :url
      t.string :team_member
      t.string :company
      t.string :slug

      t.timestamps
    end
  end
end
