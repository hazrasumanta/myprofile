class CreateEnquiries < ActiveRecord::Migration[5.1]
  def change
    create_table :enquiries do |t|
      t.string :name
      t.string :email
      t.integer :phone_no
      t.text :desc

      t.timestamps
    end
  end
end
