class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :c_nme
      t.string :c_date

      t.timestamps
    end
  end
end
