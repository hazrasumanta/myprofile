class AddColumnToBlogPostShortdesc < ActiveRecord::Migration[5.1]
  def change
    add_column :blog_posts, :short_desc, :text
  end
end
