class CreateBlogImages < ActiveRecord::Migration[5.1]
  def change
    create_table :blog_images do |t|
      t.string :b_id
      t.attachment :pic
      t.references :blog_posts, foreign_key: true

      t.timestamps
    end
  end
end
