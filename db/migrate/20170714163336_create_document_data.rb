class CreateDocumentData < ActiveRecord::Migration[5.1]
  def change
    create_table :document_data do |t|
      t.attachment :document
      t.boolean :status

      t.timestamps
    end
  end
end
