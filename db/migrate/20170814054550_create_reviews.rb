class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.string :name
      t.string :email
      t.text :content
      t.boolean :display_statas
      t.boolean :aproval_status

      t.timestamps
    end
  end
end
