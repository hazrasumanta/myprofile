class CreateTechnologies < ActiveRecord::Migration[5.1]
  def change
    create_table :technologies do |t|
      t.string :name
      t.string :tech_type
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
