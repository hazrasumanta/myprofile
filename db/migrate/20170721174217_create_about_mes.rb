class CreateAboutMes < ActiveRecord::Migration[5.1]
  def change
    create_table :about_mes do |t|
      t.string :title
      t.text :desc
      t.boolean :status

      t.timestamps
    end
  end
end
