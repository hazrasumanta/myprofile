class CreateStaticPageImages < ActiveRecord::Migration[5.1]
  def change
    create_table :static_page_images do |t|
      t.string :img_type
      t.attachment :img_url
      t.boolean :status

      t.timestamps
    end
  end
end
