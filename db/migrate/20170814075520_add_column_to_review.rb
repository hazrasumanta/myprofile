class AddColumnToReview < ActiveRecord::Migration[5.1]
  def change
    add_column :reviews, :blog_post_id, :integer
  end
end
