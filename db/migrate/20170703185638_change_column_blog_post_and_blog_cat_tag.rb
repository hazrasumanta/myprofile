class ChangeColumnBlogPostAndBlogCatTag < ActiveRecord::Migration[5.1]
  def change
  	rename_column :blog_cat_tags_posts, :blog_posts_id, :blog_post_id
  	rename_column :blog_cat_tags_posts, :blog_cat_tags_id, :blog_cat_tag_id
  end
end
