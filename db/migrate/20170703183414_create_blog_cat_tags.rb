class CreateBlogCatTags < ActiveRecord::Migration[5.1]
  def change
    create_table :blog_cat_tags do |t|
      t.string :name
      t.string :c_type
      t.timestamps
    end
  end
end
