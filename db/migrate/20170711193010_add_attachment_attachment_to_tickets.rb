class AddAttachmentAttachmentToTickets < ActiveRecord::Migration[5.1]
  def self.up
    change_table :blog_posts do |t|
      t.attachment :cover_pic
    end
  end

  def self.down
    remove_attachment :blog_posts, :attachment
  end
end
