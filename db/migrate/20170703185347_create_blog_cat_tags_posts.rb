class CreateBlogCatTagsPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :blog_cat_tags_posts do |t|
      t.references :blog_posts, foreign_key: true
      t.references :blog_cat_tags, foreign_key: true

      t.timestamps
    end
  end
end
