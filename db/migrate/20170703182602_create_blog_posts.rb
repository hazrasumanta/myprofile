class CreateBlogPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :blog_posts do |t|
      t.string :title
      t.text :desc
      t.string :slug
      t.boolean :status

      t.timestamps
    end
  end
end
