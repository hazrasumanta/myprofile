class ChangeColumnToEnquiry < ActiveRecord::Migration[5.1]
  def change
  	change_column :enquiries, :phone_no, :integer , :limit=> 8
  end
end
