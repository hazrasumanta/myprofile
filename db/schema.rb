# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170814075520) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "about_mes", force: :cascade do |t|
    t.string "title"
    t.text "desc"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "blog_cat_tags", force: :cascade do |t|
    t.string "name"
    t.string "c_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "blog_cat_tags_posts", force: :cascade do |t|
    t.bigint "blog_post_id"
    t.bigint "blog_cat_tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blog_cat_tag_id"], name: "index_blog_cat_tags_posts_on_blog_cat_tag_id"
    t.index ["blog_post_id"], name: "index_blog_cat_tags_posts_on_blog_post_id"
  end

  create_table "blog_images", force: :cascade do |t|
    t.string "blog_post_id"
    t.string "pic_file_name"
    t.string "pic_content_type"
    t.integer "pic_file_size"
    t.datetime "pic_updated_at"
    t.bigint "blog_posts_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blog_posts_id"], name: "index_blog_images_on_blog_posts_id"
  end

  create_table "blog_posts", force: :cascade do |t|
    t.string "title"
    t.text "desc"
    t.string "slug"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "view_count"
    t.string "cover_pic_file_name"
    t.string "cover_pic_content_type"
    t.integer "cover_pic_file_size"
    t.datetime "cover_pic_updated_at"
    t.text "short_desc"
  end

  create_table "blog_subscribers", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "data_fingerprint"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "c_nme"
    t.text "c_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "document_data", force: :cascade do |t|
    t.string "document_file_name"
    t.string "document_content_type"
    t.integer "document_file_size"
    t.datetime "document_updated_at"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enquiries", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.bigint "phone_no"
    t.text "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "homes", force: :cascade do |t|
    t.string "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.string "pic_file_name"
    t.string "pic_content_type"
    t.integer "pic_file_size"
    t.datetime "pic_updated_at"
    t.string "duration"
    t.text "desc"
    t.string "url"
    t.string "team_member"
    t.string "company"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.text "content"
    t.boolean "display_statas"
    t.boolean "aproval_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "blog_post_id"
  end

  create_table "static_page_images", force: :cascade do |t|
    t.string "img_type"
    t.string "img_url_file_name"
    t.string "img_url_content_type"
    t.integer "img_url_file_size"
    t.datetime "img_url_updated_at"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "technologies", force: :cascade do |t|
    t.string "name"
    t.string "tech_type"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_technologies_on_project_id"
  end

  add_foreign_key "blog_cat_tags_posts", "blog_cat_tags"
  add_foreign_key "blog_cat_tags_posts", "blog_posts"
  add_foreign_key "blog_images", "blog_posts", column: "blog_posts_id"
  add_foreign_key "technologies", "projects"
end
