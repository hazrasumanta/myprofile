Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  
  root 'home#index'
  #home controller routes besically for home static page
  resources :contact
	resources :home do
		collection do
			# get :quick_view
		end
	end

  
  #routes for about
  resources :abouts, :path =>"/about-me"
  #routes for blog
  resources :blogs do
    collection do
      get :get_list
      get :get_cat
      get :new_subscriber
      get :model
      post :review_create
    end
    member do
      get :tag
    end
  end
  #routes for projects
  resources :project do
    collection do
      # get :quick_view
    end
  end

  get "/quick_view"=>"home#quick_view",:as=>'quick_view'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
