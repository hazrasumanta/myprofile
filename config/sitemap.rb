# Set the host name for URL creation
#SitemapGenerator::Sitemap.sitemaps_path = public_path
SitemapGenerator::Sitemap.default_host = "http://www.sumantahazra.info"
SitemapGenerator::Sitemap.create_index = true
SitemapGenerator::Sitemap.create do
  #sitemap link to all different sitemap file
  # add "/sitemaps/blog.xml.gz", :changefreq => 'weekly'
  # add "/sitemaps/blog_tags.xml.gz", :changefreq => 'weekly'
  # add "/sitemaps/project.xml.gz", :changefreq => 'weekly'
  
  #static pages sitemap like home, contace, about-me
  group(:filename => :static_pages, :sitemaps_path => 'sitemaps/') do
    add root_path, :changefreq => 'daily' 
    add abouts_path, :changefreq => 'weekly'
    add "/contact", :changefreq => 'weekly'
  end
  #blog main pages sitemap
  group(:filename => :blog, :sitemaps_path => 'sitemaps/') do
    add "/blogs", :changefreq => 'weekly'
    BlogPost.find_each do |post|
      add blog_path(post), :changefreq => 'weekly', :lastmod => post.updated_at
    end
  end
  #blog tag pages sitemap
  group(:filename => :blog_tags, :sitemaps_path => 'sitemaps/') do
    BlogCatTag.find_each do |tag|
      add tag_blog_path(tag), :changefreq => 'weekly', :lastmod => tag.updated_at
    end
  end
  #projects sitemap pages
  group(:filename => :projects, :sitemaps_path => 'sitemaps/') do
    add "/project", :changefreq => 'weekly'
    Project.find_each do |pro|
      add project_path(pro),  :changefreq => 'weekly', :lastmod => pro.updated_at
    end
  end
end
