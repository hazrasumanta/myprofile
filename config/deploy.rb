# config valid only for current version of Capistrano
lock "3.9.0"
set :application, "Myprofile"
set :repo_url, "git@bitbucket.org:hazrasumanta/myprofile.git"
set :branch, 'master'
set :user, 'sumanta'

set :deploy_to, "/home/sumanta/www/project"
set :ssh_options, { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa) }

set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp', 'public/system', "public/downloads", "public/assets")
#set :sitemap_roles, :web


namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end
  before :start, :make_dirs
end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end

  task :update_crontab do
    on roles(:all) do
      within current_path do
        execute :bundle, :exec, :whenever, "--update-crontab", "/home/sumanta/www/project/current/config/schedule.rb"
        end
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  #after 'deploy:symlink:release', 'deploy:update_crontab'
  #after  :finishing,    :restart
end

namespace :solr do
  %w[start stop].each do |command|
    desc "#{command} solr"
    task command do
      on roles(:app) do
        solr_pid = "#{shared_path}/tmp/pids/sunspot-solr-production.pid"
        if command == "start" or (test "[ -f #{solr_pid} ]" and test "kill -0 $( cat #{solr_pid} )")
          within current_path do
            with rails_env: fetch(:rails_env, 'production') do
              execute :bundle, 'exec','rake', "sunspot:solr:#{command}", 'RAILS_ENV=production'
            end
          end
        end
      end
    end
  end

  desc "restart solr"
  task :restart do
    invoke 'solr:stop'
    invoke 'solr:start'
  end

  after 'deploy:finished', 'solr:restart'

  desc "reindex the whole solr database"

  task :reindex do
    invoke 'solr:stop'
    on roles(:app) do
      execute :rm, "-rf #{shared_path}/solr/data"
    end
    invoke 'solr:start'
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env, 'production') do
          info "Reindexing Solr database"
          execute :bundle, 'exec', :rake, 'sunspot:solr:reindex[,,true]'
        end
      end
    end
  end

end

# namespace :sitemap do
#   desc "Generate sitemap.xml.gz"
#   task :generate do
#     on roles(:app) do
#       run "cd #{deploy_to}/current && /usr/bin/env bundle exec rake sitemap:refresh RAILS_ENV=#{rails_env}"
#     end
#   end
#   after "deploy:restart", "sitemap:generate"
# end
# namespace :sitemap do
#   desc 'Create sitemap and ping search engines'
#   task :refresh do
#     on roles fetch(:sitemap_roles, :web) do
#       within release_path do
#         with rails_env: (fetch(:rails_env) || fetch(:stage)) do
#          execute :rake, "sitemap:refresh"
#         end
#       end
#     end
#   end
# end