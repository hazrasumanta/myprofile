class StaticPageImage < ApplicationRecord

has_attached_file :img_url,
  	:styles   => {
    :tiny       => "20x20",
    :thumb      => "200x200",
    :smalla     => "100x100",
    :smallb     => "150x150",
    :medium     => "240x340",
    :large      => "1280x720"
  }

   validates_attachment :img_url,
    #:presence => true,
    :size => { :in => 0..10.megabytes },
    :content_type => { :content_type => /^image\/(jpeg|png|gif|tiff)$/ }
end
