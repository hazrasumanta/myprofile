class BlogPost < ApplicationRecord
	# before_save :view_count
	has_many :blog_images
	has_many :reviews, foreign_key: "blog_post_id"
	has_and_belongs_to_many :blog_cat_tags
	  has_attached_file :cover_pic,
	  	:styles   => {
	    :tiny       => "20x20",
	    :thumb      => "200x200",
	    :smalla     => "100x100",
	    :smallb     => "150x150",
	    :medium     => "240x340",
     	:pslide     => "500x300",
	    :large      => "1280x720"
  	}
  	 validates_attachment :cover_pic,
    #:presence => true,
    :size => { :in => 0..10.megabytes },
    :content_type => { :content_type => /^image\/(jpeg|png|gif|tiff)$/ }
	extend FriendlyId
	  friendly_id :title, use: [:slugged,:history]
	  def should_generate_new_friendly_id?
	    title_changed?
	  end
	  def set_blog_view()
	  	puts "test sumanta"
	  	if self.view_count != nil
	  		v_c = self.view_count
	  		puts "hello check this #{v_c}"
		  	self.update_column(:view_count, v_c + 1)
		  	puts "after upadte #{self.view_count}"
		  else
		  	self.update_column(:view_count,  1)
		 end
	  end

	searchable do
	   text :title , :short_desc
	   integer :id
	   date :created_at
	   boolean :status
	   text :blog_cat_tags do
		   blog_cat_tags.map(&:name)
		 end
		 text :reviews do 
		 	self.reviews.map(&:name)
		 end
		  text :reviews do 
		 	self.reviews.map(&:content)
		end
	end
end
