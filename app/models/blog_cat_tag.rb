class BlogCatTag < ApplicationRecord
  has_and_belongs_to_many :blog_posts
  extend FriendlyId
  friendly_id :name, use: [:slugged,:history]
  def should_generate_new_friendly_id?
    name_changed?
  end
end
