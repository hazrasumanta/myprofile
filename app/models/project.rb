class Project < ApplicationRecord
	has_many :technologys
	extend FriendlyId
  friendly_id :m_slug, use: [:slugged,:history]
  def should_generate_new_friendly_id?
    name_changed?
  end
  def m_slug
  	url = ''
  	if self.name.present?
  		url = url + self.name
  		all_tech = Technology.where("project_id = ?",self.id)
  		all_tech.each do |x|
  			url = url + x.name
  		end
  		url
  	else
  		self.name
  	end
 	

  end
	has_attached_file :pic,
	  	:styles   => {
	    :tiny       => "20x20",
	    :thumb      => "200x200",
	    :smalla     => "100x100",
	    :smallb     => "150x150",
	    :medium     => "240x340",
      :pslide     => "500x300",
	    :large      => "1280x720"
  }

   validates_attachment :pic,
    #:presence => true,
    :size => { :in => 0..10.megabytes },
    :content_type => { :content_type => /^image\/(jpeg|png|gif|tiff)$/ }
end
