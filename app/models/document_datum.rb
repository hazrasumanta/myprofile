class DocumentDatum < ApplicationRecord
	has_attached_file :document
    

	validates_attachment :document,:size => { :in => 0..10.megabytes }, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }
end
