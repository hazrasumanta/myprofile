class ContactController < ApplicationController
  before_action :authorize, only: [:edit, :update, :new, :destroy]
  def index
  	@title= 'SH profile - Contact'
  	@new_enq = Enquiry.new
    set_meta_tags image_src: "http://www.sumantahazra.info/assets/pofile_pic.jpg"
    set_meta_tags description: "Sumanta Hazra Contact +91-9040451287, +91-9066627015, +91-9153097405, amailtosumanta@gmail.com ,hazrasumanta@gmail.com"
    
    set_meta_tags og: {
      title:    "Sumanta Contact",
      type:     'Contact',
      site_name: "Sumanta Hazra contact",
      url:      "http://www.sumantahazra.info/contact",
      image:    "#{ActionController::Base.helpers.asset_url('pofile_pic.jpg')}",
      description: "Sumanta Hazra Contact +91-9040451287, +91-9066627015, +91-9153097405, amailtosumanta@gmail.com ,hazrasumanta@gmail.com"
      # video:    {
      # director: 'http://www.imdb.com/name/nm0000881/',
      # writer:   ['http://www.imdb.com/name/nm0918711/', 'http://www.imdb.com/name/nm0177018/']
      # }
    }
    set_meta_tags twitter: {
      card:  "Contact",
      description: "Sumanta Hazra Contact +91-9040451287, +91-9066627015, +91-9153097405, amailtosumanta@gmail.com ,hazrasumanta@gmail.com",
      title: "Sumanta Contact",
        image: {
        _:      "#{ActionController::Base.helpers.asset_url('pofile_pic.jpg')}",
        width:  100,
        height: 100,
        }
    }
  end

  def create
  	@new_enq = Enquiry.new(enruiry_params)
    if !verify_recaptcha(model: @new_enq) || !@new_enq.save
      flash[:danger] = "Enquiry not Create Successfully!! Please click on reCapture"
      redirect_to "/contact"
    else
      @new_enq.save
  		flash[:success] = "Enquiry Created Successfully"
  		ApplicationMailer.send_email(@new_enq.email,"Enquiry mail",@new_enq.phone_no,@new_enq.desc,@new_enq.created_at).deliver
      redirect_to "/"
  	end
    



  end
  private
  	def enruiry_params
      params.require(:enquiry).permit(:name, :email, :phone_no, :desc)
    end
end
