class AboutsController < ApplicationController
  before_action :set_about, only: [:show, :edit, :update, :destroy]
  before_action :authorize, only: [:edit, :update, :new, :destroy]
  # GET /abouts
  # GET /abouts.json
  def index
    @title= 'SH - About Me'
    set_meta_tags description: "Hi!! I am Sumanta Hazra. I am from West Bengal(West Midnapore, Daspur). I born in early 90's 1st June. Here you can get to know all my basic details."
    @all_about = AboutMe.where("status = ?",true )
    @first_div = AboutMe.where("title = ?","About me").last
    set_meta_tags keywords: %w[About Section, early life, Early life and family]
  end

  # GET /abouts/1
  # GET /abouts/1.json
  def show
    @title= 'SH - About Me Show'
  end

  # GET /abouts/new
  def new
    @title= 'SH - About Me New'
    @about = AboutMe.new
  end

  # GET /abouts/1/edit
  def edit
    @title= 'SH - About Me Edit'
  end

  # POST /abouts
  # POST /abouts.json
  def create
    @about = AboutMe.new(about_params)

    respond_to do |format|
      if @about.save
        format.html { redirect_to about_path(@about), notice: 'About was successfully created.' }
        #format.json { render :show, status: :created, location: @about }
      else
        format.html { render :new }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /abouts/1
  # PATCH/PUT /abouts/1.json
  def update
    respond_to do |format|
      if @about.update(about_params)
        format.html { redirect_to about_path(@about), notice: 'About was successfully updated.' }
        format.json { render :show, status: :ok, location: @about }
      else
        format.html { render :edit }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /abouts/1
  # DELETE /abouts/1.json
  def destroy
    @about.destroy
    respond_to do |format|
      format.html { redirect_to abouts_url, notice: 'About was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_about
      @about = AboutMe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def about_params
      params.fetch(:about_me, {}).permit(:title,:desc,:status)
    end
end
