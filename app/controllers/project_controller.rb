class ProjectController < ApplicationController
	before_action :authorize, only: [:edit, :update, :new, :destroy]
  def index
  	@all_products = Project.where('id is not null').order('id DESC')
    @title= 'SH profile - PROJECT- INDEX'
    set_meta_tags description: "This is a section where I am sharing my projects list. All projects are basically web app. Ruby on Rails , Sample project design"
  end

  def show
  	@project = Project.friendly.find(params[:id])
  	if @project.present?
  		@all_technology = @project.technologys
  	end
    set_meta_tags description: "#{@project.desc}"
    set_meta_tags keywords: @project.technologys.pluck(:name)
    set_meta_tags canonical: "http://www.sumantahazra.info/project"
    #@blogs_populer = BlogPost.where("status = ?",true).order('view_count DESC').page params[:page]
    set_meta_tags og: {
      title:    @project.name,
      type:     'Articles',
      url:      "http://www.sumantahazra.info/project/#{@project.slug}",
      description: "#{@project.desc}"
      #image:     @blogs_list.first.cover_pic.url(:thumb)
      # video:    {
      # director: 'http://www.imdb.com/name/nm0000881/',
      # writer:   ['http://www.imdb.com/name/nm0918711/', 'http://www.imdb.com/name/nm0177018/']
      # }
    }
    set_meta_tags image_src: @project.pic.url(:thumb)
    set_meta_tags author: "Sumanta Hazra"
     set_meta_tags twitter: {
      card:  "Project",
      description: "#{@project.desc}",
      title: "#{@project.name}",
        # image: {
        # _:      @blog.cover_pic.url(:thumb),
        # width:  100,
        # height: 100,
        # }
    }
    set_meta_tags article: {
      section:           'Sumanta Projects',
      tag:               "Tech Projects"
    }
  end
end
