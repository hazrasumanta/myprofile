class BlogsController < ApplicationController
  include BlogSolr
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  before_action :authorize, only: [:edit, :update, :new, :destroy]
  # before_action :set_blog_view, only:[:show]
 # add_breadcrumb "home", :root_path
 #before_save :hello

 # def set_blog_view
 #    BlogPost.set_view_count()
 # end
  # GET /blogs
  # GET /blogs.json
  def index
    @title= 'SH - Blog - Index'
    @subscriber = BlogSubscriber.new
    if params[:search].present?
      @search = search_solr(params[:search])
      @blogs_list = @search.results
      @scarch_text = params[:search]
    else
      @blogs_list = BlogPost.where("status = ?",true).order('view_count DESC').paginate(:page => params[:page], :per_page => 5)
      #@blogs_list = BlogPost.where('created_at >= ? AND status = ?', Time.now-30.days, true).order("id desc").paginate(:page => params[:page], :per_page => 5)
    end
    #@blogs_list = Kaminari.paginate_array(@blogs_list).page(params[:page]).per(1)
    @blog_paralux_img = StaticPageImage.where("img_type=?","blog_paralax").last
    set_meta_tags description: "Sumanta Hazra Ruby on Rails Developer. Blog is a section here i write blogs based on new technologies witch i learn thorugh my work"
    set_meta_tags keywords: %w[Blog Sumantas_Blog Ruby Rails CSS JAVAScript Jquiry Web Bootstrap Ruby_On_Rails_Blog]
    set_meta_tags canonical: "http://www.sumantahazra.info/blogs"
    #@blogs_populer = BlogPost.where("status = ?",true).order('view_count DESC').page params[:page]
    set_meta_tags og: {
      title:    'SH - Blog - Index',
      type:     'Blog index',
      url:      "http://www.sumantahazra.info",
      description: "Sumanta Hazra Ruby on Rails Developer. Blog is a section here i write blogs based on new technologies witch i learn thorugh my work"
      #image:     @blogs_list.first.cover_pic.url(:thumb)
      # video:    {
      # director: 'http://www.imdb.com/name/nm0000881/',
      # writer:   ['http://www.imdb.com/name/nm0918711/', 'http://www.imdb.com/name/nm0177018/']
      # }
    }
    set_meta_tags image_src: "#{ActionController::Base.helpers.asset_url('pofile_pic.jpg')}"
    set_meta_tags author: "Sumanta Hazra"
     set_meta_tags twitter: {
      card:  "Blog",
      description: "Sumanta Hazra Ruby on Rails Developer. Blog is a section here i write blogs based on new technologies witch i learn thorugh my work",
      title: "SH - Blog - Index",
        # image: {
        # _:      @blog.cover_pic.url(:thumb),
        # width:  100,
        # height: 100,
        # }
    }
    set_meta_tags article: {
      section:           'Sumanta Blog',
      tag:               "Tech blog"
    }
    respond_to do |f|
      f.html
      f.js
    end
  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
    @title= 'SH - Blog - '+@blog.title
    set_meta_tags description:  @blog.short_desc
    set_meta_tags author: "Sumanta Hazra"
    set_meta_tags image_src: @blog.cover_pic.url(:thumb)
    set_meta_tags canonical: "http://www.sumantahazra.info/blogs/#{@blog.slug}"
    @blog.set_blog_view()
    set_meta_tags og: {
      title:    @blog.title,
      type:     'Blog, Article',
      site_name: "Sumanta Hazra info And Blog",
      url:      "http://www.sumantahazra.info/blogs/#{@blog.slug}",
      image:    @blog.cover_pic.url(:thumb),
      description: @blog.short_desc
      # video:    {
      # director: 'http://www.imdb.com/name/nm0000881/',
      # writer:   ['http://www.imdb.com/name/nm0918711/', 'http://www.imdb.com/name/nm0177018/']
      # }
    }
    set_meta_tags twitter: {
      card:  "Blog",
      description: @blog.short_desc,
      title: @blog.title,
        image: {
        _:      @blog.cover_pic.url(:thumb),
        width:  100,
        height: 100,
        }
    }
    set_meta_tags article: {
      published_time:    @blog.created_at,
      modified_time:     @blog.updated_at,
      section:           'Sumanta Blog',
      tag:               "Tech blog"
    }
    @review_new = Review.new
    @my_choice = BlogPost.where('created_at >= ? AND status = ?', Time.now-2.days, true).limit(10)
    @populer_blog = @blogs_list = BlogPost.where("status = ?",true).order('view_count DESC').limit(10)
  end

  # GET /blogs/new
  def new
    @blog = BlogPost.new
    @blog.blog_images.build
    set_meta_tags nofollow: 'googlebot'
    set_meta_tags nofollow: true
  end

  #review save
  def review_create
    if params[:review][:blog_post_id].present?
     @blog = BlogPost.find(params[:review][:blog_post_id])
      if verify_recaptcha(model: @blog) && @blog.reviews.create(review_params)
        flash[:success] = "Thank you for review. Your review require approval"
        redirect_back fallback_location: root_path
      else
        flash[:error] = "Sorry!! review can not be create now"
        redirect_back fallback_location: root_path
      end
    else
      flash[:error] = "Sorry!! review can not be create now"
      redirect_back fallback_location: root_path
    end
  end

  #model popup
  def model
    puts "model js"
    @review_new = Review.new
    @blog_id = params[:b_id] if params[:b_id].present?
    @blog = BlogPost.find(@blog_id) if @blog_id.present?
    @title = params[:value]
    respond_to do |format|
      format.js #If it's a js request this line tell rails to look for new_release.js.erb in your views directory
    end
  end

  #new subscriber

  def new_subscriber
    puts "hello sumanta ===================>"
    if params[:name].present? && params[:email].present?
      name = params[:name]
      email = params[:email]
      @sub_check = BlogSubscriber.where("email = ?",params[:email]).last
      if @sub_check.blank?
        new_subscriber = BlogSubscriber.new
        new_subscriber.name = name
        new_subscriber.email = email
        if new_subscriber.save
          @result = true
          ApplicationMailer.send_subscriber_email(new_subscriber.email,"Blog Subscribtion confirmation mail",new_subscriber.name).deliver
        end
      else
        @result = false
      end
    end
  end
  #tag paths
  def tag
    @tag =  BlogCatTag.friendly.find(params[:id])
    @title= "SH - Blog - Scarch by #{@tag.name}"
    @blogs_list = @tag.blog_posts.paginate(:page => params[:page], :per_page => 5)
     set_meta_tags description: "Sumanta Hazra Ruby on Rails Developer. Blog is a section here i write blogs based on new technologies witch i learn thorugh my work: tag #{@tag.name}"
    set_meta_tags keywords: %w[Blog Sumantas_Blog Ruby Rails CSS JAVAScript Jquiry Web Bootstrap Ruby_On_Rails_Blog]
    set_meta_tags canonical: "http://www.sumantahazra.info/blogs"
    #@blogs_populer = BlogPost.where("status = ?",true).order('view_count DESC').page params[:page]
    set_meta_tags og: {
      title:    "SH - Blog - Scarch by #{@tag.name}",
      type:     'Blog index',
      url:      "http://www.sumantahazra.info",
      description: "Sumanta Hazra Ruby on Rails Developer. Blog is a section here i write blogs based on new technologies witch i learn thorugh my work: Scarch by #{@tag.name}"
      #image:     @blogs_list.first.cover_pic.url(:thumb)
      # video:    {
      # director: 'http://www.imdb.com/name/nm0000881/',
      # writer:   ['http://www.imdb.com/name/nm0918711/', 'http://www.imdb.com/name/nm0177018/']
      # }
    }
    set_meta_tags image_src: "http://www.sumantahazra.info/assets/pofile_pic.jpg"
    set_meta_tags author: "Sumanta Hazra"
     set_meta_tags twitter: {
      card:  "Blog",
      description: "Sumanta Hazra Ruby on Rails Developer. Blog is a section here i write blogs based on new technologies witch i learn thorugh my work",
      title: "SH - Blog - Scarch by #{@tag.name}",
        # image: {
        # _:      @blog.cover_pic.url(:thumb),
        # width:  100,
        # height: 100,
        # }
    }
    set_meta_tags article: {
      section:           'Sumanta Blog',
      tag:               "Tech blog"
    }
  end


  #get blog list

  def get_list
    if params[:blog_req_type] == "recent"
      @blogs_list = BlogPost.where('created_at >= ? AND status = ?', Time.now-30.days, true).order("id desc").paginate(:page => params[:page], :per_page => 5)#.page params[:page]
        @pagenate = params[:paginate]
      #@test = "recent"
    elsif params[:blog_req_type] == "populer"
      puts "in populer"
      @blogs_list = BlogPost.where("status = ?",true).order('view_count DESC').paginate(:page => params[:page], :per_page => 5)#.page params[:page]
      #@test = "populer"
      @pagenate = params[:paginate]
    end
  end
  # GET /blogs/1/edit
  def edit
  end


   def get_cat
    exams = BlogCatTag.select([:name,:id]).where("name like ? AND c_type=?","%#{params[:name]}%","tag").limit(10)
    result = exams.collect do |t|
      {
        nm: t.name,
        id: t.id
      }
    end
     respond_to do |format|
      format.json {render :json=>{:results=>result}}
    end
    end
  # POST /blogs
  # POST /blogs.json
  def create
    @blog = BlogPost.new(blog_params)
    # blog_cove_img = BlogImage.new
    # blog_cove_img.pic = open(params[:blog_cover_pic])
    #blog_cove_img.save
    #@blog.blog_images << params[:pic]
    @added_tags = BlogCatTag.where("id in (?)",params[:tags])
    @blog.blog_cat_tags << @added_tags
    respond_to do |format|
      if @blog.save
        @blog.update_column(:status, true)
        format.html { redirect_to blog_path(@blog), notice: 'Blog was successfully created.' }
        format.json { render :show, status: :created, location: @blog }
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
    set_meta_tags nofollow: 'googlebot'
    set_meta_tags nofollow: true
  end

  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to blog_path(@blog), notice: 'Blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
    set_meta_tags nofollow: 'googlebot'
    set_meta_tags nofollow: true
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to blogs_url, notice: 'Blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      @blog = BlogPost.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
       params.fetch(:review, {}).permit(:name,:content,:display_statas,:email,:blog_post_id)
    end
    def blog_params
      params.fetch(:blog_post, {}).permit(:title,:desc,:tags,:cover_pic,:short_desc)
    end
end
