class HomeController < ApplicationController
  before_action :authorize, only: [:edit, :update, :new, :destroy]
	#before_action :set_layout
	# def set_layout
	# 	if !admin_user_signed_in?
	# 	  self.class.layout false 
	# 	else
	# 	  self.class.layout 'application'
	# 	end
	# end
  def index
  	@title= 'Sumanta Hazra profile - Home'
  	@all_products = Project.where('id is not null').order('id DESC')
  	@all_img =  StaticPageImage.where("id is not null")
    @populer_blog = BlogPost.where("status = ?",true).order('view_count DESC').limit(6)
    @doc_cv = DocumentDatum.first
    set_meta_tags image_src: "#{ActionController::Base.helpers.asset_url('pofile_pic.jpg')}"
    set_meta_tags description: "I am a Web App developer. I build basically web apps based on Ruby On Rails."
      set_meta_tags og: {
      title:    'SH profile - Home',
      type:     'Sumanta Info',
      url:      "http://www.sumantahazra.info",
      description: "I am a Web App developer. I build basically web apps based on Ruby On Rails.",
      image:    "#{ActionController::Base.helpers.asset_url('pofile_pic.jpg')}"
      # video:    {
      # director: 'http://www.imdb.com/name/nm0000881/',
      # writer:   ['http://www.imdb.com/name/nm0918711/', 'http://www.imdb.com/name/nm0177018/']
      # }
    }
    set_meta_tags author: "Sumanta Hazra"
     set_meta_tags twitter: {
      card:  "Info",
      description: "I am a Web App developer. I build basically web apps based on Ruby On Rails.",
      title: "SH profile - Home",
        image: {
        _:      "#{ActionController::Base.helpers.asset_url('pofile_pic.jpg')}",
        width:  100,
        height: 100,
        }
    }
    set_meta_tags article: {
      section:           'Sumanta Hazra Info',
      tag:               "Sumanta Hazra Info || Blog"
    }

  end
  def quick_view
  	puts "helooooooooooooooooooooooooooooooooooooo"
  	@doc_cv = DocumentDatum.first
  end
end
