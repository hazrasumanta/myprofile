class ApplicationMailer < ActionMailer::Base
  default from: 'hazrasumanta@gmail.com'
  layout 'mailer'
  def send_email(mail,subject,phone_no,desc,create_time)
	   @phone_no = phone_no
	   @email = mail
	   @desc = desc
     @create_time = create_time
	   mail(:to=>mail,:bcc=>"hazrasumanta@gmail.com", :subject=>subject)
  end
  def send_subscriber_email(mail_id,mail_type,name)
  	@email = mail_id
  	@name = name
  	mail(:to=>mail_id,:bcc=>"hazrasumanta@gmail.com", :subject=>mail_type)
  end
   def test_email()
	  
	   mail(:to=>"amailtosumanta@gmail.com",:bcc=>"hazrasumanta@gmail.com", :subject=>"test mail")
  end

end
